package com.errigal.util;
import com.errigal.bo.Network;
import com.google.gson.Gson;
/**
 * Created by avi on 09/06/2017.
 */
public class JSONSerializer {

    public static String Serialize(Network network){

        Gson gson = new Gson();
        String jsonStr= gson.toJson(network);
        return jsonStr;
    }
}

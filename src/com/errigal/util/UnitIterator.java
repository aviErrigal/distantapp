package com.errigal.util;

import com.errigal.bo.*;

/**
 * Created by avi on 08/06/2017.
 */
public class UnitIterator {


    public static void Iterate(Unit unit,boolean showUnit, boolean showAlarm, boolean showRemedy){


        if(showUnit) {
            System.out.println(unit.getClass().getSimpleName()+" UID: " + unit.getUID() + " Name: " + unit.getName());
        }

        if(unit instanceof Network){

            ((Network) unit).getCarriers().forEach((carrier -> {


                Iterate(carrier,showUnit,showAlarm,showRemedy);

            }));

        }else if(unit instanceof Carrier){

            Carrier carrier = (Carrier) unit;
            carrier.getHubs().forEach((hub)->{


                Iterate(hub, showUnit, showAlarm, showRemedy);


            });
        }else if(unit instanceof Hub){

            Hub hub = (Hub) unit;
            if(showAlarm) {
                showAlarms(hub);
            }

            if(showRemedy){
                showRemedies(hub);

            }

            hub.getNodes().forEach((node)->{



                Iterate(node, showUnit, showAlarm, showRemedy);


            });
        }else if(unit instanceof Node){

            Node node = (Node) unit;


            if(showAlarm) {
                showAlarms(node);
            }

            if(showRemedy){
                showRemedies(node);

            }


        }

    }

    public static void showAlarms(ObservableUnit unit){

        if( unit.getAlarms().size()==0){
            //System.out.println("    |___ No alarms found for this unit.");
            return;
        }
        unit.getAlarms().forEach((String k, Alarm alarm) ->{

            System.out.println("    |___ UID: "+k+" alarmType: "+alarm.getKey());
        });
    }

    public static void showRemedies(ObservableUnit unit){

        if( unit.getAlarms().size()==0){
            //System.out.println("    |___ No Remedies found for this unit.");
            return;
        }
        unit.getAlarms().forEach((String k, Alarm alarm) ->{

            System.out.println("    |___ Remedy: "+alarm.getValue());
        });
    }
}

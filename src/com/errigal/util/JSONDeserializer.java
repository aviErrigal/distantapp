package com.errigal.util;
import com.errigal.bo.*;
import com.google.gson.Gson;

/**
 * Created by avi on 09/06/2017.
 */
public class JSONDeserializer {

    public static Network Deserialize(String jsonString){

        Gson gson = new Gson();
        Network network = gson.fromJson(jsonString,Network.class);
        UIDStore.reset();
        loadToMemory(network);
        return network;
    }

    public static void loadToMemory(Unit unit){


        UIDStore.addToStore(unit.getUID(),unit);

        if(unit instanceof Network){

            ((Network) unit).getCarriers().forEach((carrier -> {


                loadToMemory(carrier);

            }));

        }else if(unit instanceof Carrier){

            Carrier carrier = (Carrier) unit;
            carrier.getHubs().forEach((hub)->{


                loadToMemory(hub);


            });
        }else if(unit instanceof Hub){

            Hub hub = (Hub) unit;
            loadAlarms(hub);

            hub.getNodes().forEach((node)->{



                loadToMemory(node);


            });
        }else if(unit instanceof Node){

                Node node = (Node) unit;
                loadAlarms(node);
        }


    }

    public static void loadAlarms(ObservableUnit unit){

        if( unit.getAlarms().size()==0){
            //System.out.println("    |___ No alarms found for this unit.");
            return;
        }
        unit.getAlarms().forEach((String k, Alarm alarm) ->{

            UIDStore.addToStore(UIDStore.generateUID(),null);
        });
    }

}

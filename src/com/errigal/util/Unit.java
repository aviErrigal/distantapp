package com.errigal.util;

/**
 * Created by avi on 06/06/2017.
 * This is the superClass for all logical and physical units.
 */
public class Unit {

    private String UID;
    private String name;

    public String getUID(){

        return UID;
    }

    public void setUID(String uid){

        this.UID = uid;
    }

    public void setName(String name){
        this.name = name;
    }

    public String getName(){
        return this.name;
    }
}

package com.errigal.util;

import com.errigal.bo.Alarm;

import java.util.HashMap;

/**
 * Created by avi on 06/06/2017.
 * UIDStore stores Unique Identifies along with the units respectively.
 */
public class UIDStore {

    private static HashMap<String,Unit> store = new HashMap<>();
    private static long startingNumber = 123; //just a number to begin with. this can be any number.
    private static long count;


    static {
        count = startingNumber; //we use another variable just preserve the startingNumber for reset().
    }

    //for auto generated UIDs for testing and where UIDs are not specified.
    public static String generateUID(){

        String testKey = "";

        while(true){

            testKey = "AT"+count;
            if(store.containsKey(testKey)){
                count++;
            }else{
                return testKey;
            }
        }
    }

    public static void addToStore(String key,Unit value) throws IllegalArgumentException{

        if(store.containsKey(key)){
            throw new IllegalArgumentException("Given UID already exists.");
        }

        store.put(key,value);
    }

    public static void removeFromStore(String key) throws IllegalArgumentException{

        if(store.containsKey(key)){
            store.remove(key);
            return;
        }

        throw new IllegalArgumentException("Given UID does not exist.");

    }

    public static Unit getUnit(String key) throws IllegalArgumentException {

        if(store.containsKey(key)){
            return store.get(key);
        }

        throw new IllegalArgumentException("Given UID does not exist.");

    }


    public static int getStoreCount(){
        return store.size();
    }

    public static void reset(){

        store.clear();
        count = startingNumber;
    }

}

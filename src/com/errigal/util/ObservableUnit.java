package com.errigal.util;

import com.errigal.bo.Alarm;
import com.errigal.bo.Hub;
import com.errigal.bo.Node;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by avi on 06/06/2017.
 *
 *  Units that can be observed for alarms and remedies will extend this class.
 */
public class ObservableUnit extends Unit{

    private HashMap<String,Alarm> alarms = new HashMap<>();

    public void attachAlarm(String uid,Alarm alarm){

        for (Object alarmObj : alarms.values()) {

            Alarm alarmInst = (Alarm) alarmObj;
            if(alarmInst.getKey().equals(alarm.getKey())){
                //System.out.println("Similar Alarm already exists");
                return;
            }
        }

        alarms.put(uid,alarm);
        UIDStore.addToStore(uid,null);

        if(alarm.getKey().equals("Unit Unavailable")) {

            notifyAllObservers(alarm);
        }

    }

    public void notifyAllObservers(Alarm alarm){

            if(this instanceof Hub){
                Hub hub = (Hub)this;
                hub.getNodes().forEach((Node node)->{

                    System.out.println("alarm propagated to node: "+node.getName());
                    node.attachAlarm(UIDStore.generateUID(),alarm);
                });
            }
    }

    public int getAlarmCount(){

        return alarms.size();
    }


    public boolean hasAlarm(Alarm alarm){

        for (Object alarmObj : alarms.values()) {

            Alarm alarmInst = (Alarm) alarmObj;
            if(alarmInst.getKey().equals(alarm.getKey())){
                return true;
            }
        }

        return false;

    }

    public HashMap<String,Alarm> getAlarms(){
        return alarms;
    }


    public void removeAlarmByType(Alarm alarm)throws IllegalArgumentException{

        Iterator it = this.alarms.entrySet().iterator();
        while (it.hasNext())
        {
             Map.Entry pair = (Map.Entry)it.next();
             String key= (String)pair.getKey();
             Alarm alarmInst = (Alarm) pair.getValue();
            if(alarmInst.getKey().equals(alarm.getKey())){

                UIDStore.removeFromStore(key);
                it.remove();
                return;

            }

        }

        throw new IllegalArgumentException("Given AlarmType does not exist.");

    }

    public void removeAlarm(String key)throws IllegalArgumentException{

        if(alarms.containsKey(key)){
            UIDStore.removeFromStore(key);
            alarms.remove(key);
            return;
        }
        throw new IllegalArgumentException("Given UID does not exist.");

    }

    public void clearAllAlarms(){

       //clear all alarms from UIDStore First.
       this.alarms.forEach((String k, Alarm v) ->{

           UIDStore.removeFromStore(k);
       });

        this.alarms.clear();

    }

}

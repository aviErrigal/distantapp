package com.errigal.processor;

import com.errigal.bo.Network;

/**
 * Created by avi on 07/06/2017.
 */
public class QuitCmdProcessor implements CmdProcessorInterface {


    public static void process(Network n,String s){

        System.out.println("quit request completed.");
        CommandProcessor.exit = true;
    }
}

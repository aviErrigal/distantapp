package com.errigal.processor;

import com.errigal.bo.Network;

import java.lang.reflect.Method;

/**
 * Created by avi on 07/06/2017.
 */
public class CommandProcessor implements CmdProcessorInterface {

    public static boolean debugMode = false;
    public static boolean exit = false;
    public static Network network;

    public static void process(String str) throws Exception{

        if(str.isEmpty() || str.equals("")){
            return;
        }

        String cmdArray[] = str.split(" ");

        /**
         * get the primary command,
         * remove spaces and convert to lowercase as this is a human input
         */
        String cmd = cmdArray[0].trim().toLowerCase();
        String cmdCapitalized = CmdProcessorInterface.capitalize(cmd);

        /**
         * get rid of the primary command from the main cmd string.
         * Ex I/P: list alarm CarrierName/HubName
         *    O/P: alarm CarrierName/HubName
         */
        String subCmd = str.replaceFirst(cmd,"");

        // a simple Abstract Factory implementation
        try {

            Class[] paramString = new Class[2];
            paramString[0] = Network.class;
            paramString[1] = String.class;
            Class clsObj = Class.forName("com.errigal.processor."+cmdCapitalized + "CmdProcessor");
            Method callingMethod = clsObj.getDeclaredMethod("process",paramString);
            callingMethod.invoke(null,network,subCmd);

        }catch (Exception e){

            //e.printStackTrace();
            System.out.println("Invalid Command." );

            if(debugMode) {
                throw new Exception(e);
            }

        }


    }





    public static boolean setDataObject(Network nw) {
        network = nw;
        return true;
    }
}

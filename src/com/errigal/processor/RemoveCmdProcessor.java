package com.errigal.processor;

import com.errigal.bo.Network;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;

/**
 * Created by avi on 07/06/2017.
 */
public class RemoveCmdProcessor implements CmdProcessorInterface {

    public static void process(Network network,String str) throws Exception{


        String cmdArray[] = str.trim().split(" ");

        /**
         * get the primary command,
         * remove spaces and convert to lowercase as this is a human input
         * you would get: carrier|hub|node|alarm
         */
        String cmd = cmdArray[0].trim().toLowerCase();
        String cmdCapitalized = CmdProcessorInterface.capitalize(cmd);

        /**
         * get rid of the primary command from the main cmd string.
         * Ex I/P: remove alarm CarrierName/HubName
         *    O/P: alarm CarrierName/HubName
         */
        String subCmd = str.replaceFirst(cmd,"");

        // a simple Abstract Factory implementation for invoking a cmdService
        try {

            //Class[] paramString = new Class[1];
            //paramString[0] = String.class;
            Class clsObj = Class.forName("com.errigal.cmdService."+cmdCapitalized + "Service");
            Constructor c = clsObj.getConstructor(Network.class);
            Object o = c.newInstance(network);
            Method m = o.getClass().getMethod("processRemove", String.class);
            m.invoke(o, subCmd);


        }catch (Exception e){

            //e.printStackTrace();
            System.out.println("Invalid subCommand for add: "+str );
            if(CommandProcessor.debugMode){
                throw new Exception(e);
            }

        }


    }
}

package com.errigal.processor;

import com.errigal.bo.Network;
import com.errigal.util.JSONDeserializer;

import java.io.File;
import java.io.FileInputStream;


/**
 * Created by avi on 09/06/2017.
 */
public class ImportCmdProcessor implements CmdProcessorInterface {

    public static void process(Network network,String fileName) throws Exception{

        String jsonString=null;
        fileName = fileName.trim();
        byte[] data;

        FileInputStream fis;
        try{
                File file = new File(fileName);
                fis = new FileInputStream(file);
                data = new byte[(int) file.length()];
                fis.read(data);
                fis.close();
                jsonString = new String(data, "UTF-8");

        }catch (Exception e){
            System.out.println(e.getMessage());
            if(CommandProcessor.debugMode){
                throw new Exception(e);
            }

            return;
        }

        if(jsonString!=null){

            CommandProcessor.setDataObject(JSONDeserializer.Deserialize(jsonString));
        }



        System.out.println("Network imported successfully.");

    }
}

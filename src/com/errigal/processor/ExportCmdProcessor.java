package com.errigal.processor;

import com.errigal.bo.Network;
import com.errigal.util.JSONSerializer;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Created by avi on 09/06/2017.
 */
public class ExportCmdProcessor implements CmdProcessorInterface {

    public static void process(Network network,String fileName) throws Exception{

        String jsonString = JSONSerializer.Serialize(network);
        fileName = fileName.trim();

        try(
        BufferedWriter writer = new BufferedWriter(new FileWriter(fileName));

        ){
            writer.write(jsonString);
        }catch (IOException e){

            System.out.println(e.getMessage());
            if(CommandProcessor.debugMode){
                throw new Exception(e);
            }
            return;
        }

        System.out.println("Network exported successfully.");

    }
}

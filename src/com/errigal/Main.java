package com.errigal;

import com.errigal.bo.Network;
import com.errigal.processor.CommandProcessor;

import java.util.Scanner;

public class Main {



    public static void main(String[] args) throws Exception{

        //System.out.println("Hint: Type `help` for a list of commands.");

        Network network = new Network();
        CommandProcessor.setDataObject(network);

        System.out.println("Enter Command: (view Readme.md for help)");
        while(!CommandProcessor.exit){

            System.out.print("$ ");
            //accept new commands and pass it on for further processing.
            CommandProcessor.process(new Scanner(System.in).nextLine());
        }

    }
}

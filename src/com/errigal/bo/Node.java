package com.errigal.bo;

import com.errigal.util.ObservableUnit;

import java.util.ArrayList;

/**
 * The Node is the last leaf on the network tree.
 * A node belongs to a hub
 * A node can have multiple alarms
 * Created by avi on 06/06/2017.
 */
public class Node extends ObservableUnit{

    public Node(String uid,String nodeName){

        this.setUID(uid);
        this.setName(nodeName);

    }

}

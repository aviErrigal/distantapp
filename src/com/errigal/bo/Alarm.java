package com.errigal.bo;

/**
 *
 * Alarm Enumerated KeyValue.
 * USed to create Alarm Objects Associated with a Hub/Node.
 * Created by avi on 06/06/2017.
 *
 */
public enum Alarm {

        UNIT_UNAVAILABLE("Unit Unavailable", "Remedey for Unit Unavailable."),
        OPTICAL_LOSS("Optical Loss", "Remedy for Optical Loss"),
        DARK_FIBRE("Dark fibre", "Remedy for Dark fibre"),
        POWER_OUTAGE("Power Outage", "Remedy for Power Outage");

        //private final String alarmUID;
        private final String key;
        private final String value;

        Alarm(String key, String value) {
            this.key = key;
            this.value = value;
        }

        public String getKey() {
            return key;
        }
        public String getValue() {
            return value;
        }


}

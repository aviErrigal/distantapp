package com.errigal.bo;

import com.errigal.util.ObservableUnit;
import com.errigal.util.UIDStore;

import java.util.ArrayList;
import java.util.Iterator;

/**
 *
 * Hub here is a Physical Unit
 * A Carrier can have multiple Hubs and
 * A Hub can have multiple Nodes(which are physical units)
 * A Hub can have multiple Alarms attached to it.
 *
 * Created by avi on 06/06/2017.
 */

public class Hub extends ObservableUnit{

    private ArrayList<Node> nodes = new ArrayList<>();

    public Hub(String Uid,String hubName){

        this.setUID(Uid);
        this.setName(hubName);

    }


    public int getNodeCount(){

        return nodes.size();
    }

    public ArrayList<Node> getNodes(){

        return nodes;
    }

    public Node getNodeByName(String nodeName){

        //use iterators for safely removing the element.
        Iterator<Node> node = nodes.iterator();

        while(node.hasNext()){

            Node nodeObj = node.next();
            if(nodeObj.getName().equalsIgnoreCase((nodeName))){

                return nodeObj;
            }
        }

        throw new IllegalArgumentException("Node name not found.");
    }

    public void addNode(Node node) throws IllegalArgumentException{

        if(node.getName().isEmpty() || node.getName().trim().equals("")){

            throw new IllegalArgumentException("Invalid Node name.");
        }

        for (Node nodeObj:nodes) {

            if(nodeObj.getName().equalsIgnoreCase((node.getName()))){

                throw new IllegalArgumentException("Node name must be unique.");
            }
        }

        //what if node is added after a hub becomes UNIT_UNAVAILABLE?
        //then make sure node also remains UNIT_UNAVAILABLE
        if(this.hasAlarm(Alarm.UNIT_UNAVAILABLE)){
            node.attachAlarm(UIDStore.generateUID(),Alarm.UNIT_UNAVAILABLE);
        }

        UIDStore.addToStore(node.getUID(),node);
        nodes.add(node);
    }
    public void removeNode(String nodeName) throws IllegalArgumentException {

        //use iterators for safely removing the element.
        Iterator<Node> node = nodes.iterator();

        while(node.hasNext()){

            Node nodeObj = node.next();
            if(nodeObj.getName().equalsIgnoreCase((nodeName))){

                UIDStore.removeFromStore(nodeObj.getUID());
                nodeObj.clearAllAlarms();
                node.remove();
                return;
            }
        }

        throw new IllegalArgumentException("Node name not found.");
    }

    public void removeAllNodes()  {

        //use iterators for safely removing the element.
        Iterator<Node> node = nodes.iterator();

        while(node.hasNext()){

            Node nodeObj = node.next();
            UIDStore.removeFromStore(nodeObj.getUID());
            nodeObj.clearAllAlarms();
            node.remove();

        }

    }
}

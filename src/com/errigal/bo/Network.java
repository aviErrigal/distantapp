package com.errigal.bo;


import com.errigal.util.UIDStore;
import com.errigal.util.Unit;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

/**
 *
 * A network is a logical unit
 * A network can have multiple Carriers
 * Created by avi on 06/06/2017.
 */

public class Network extends Unit{

    private ArrayList<Carrier> carriers = new ArrayList<>();


    public Network(){

        this.setUID(UIDStore.generateUID());
        this.setName("ROOT_NET");
        UIDStore.addToStore(this.getUID(),this);
    }


    public void addCarrier(Carrier carrier) throws IllegalArgumentException{

        if(carrier.getName().isEmpty() || carrier.getName().trim().equals("")){

            throw new IllegalArgumentException("Invalid Carrier name.");
        }

        for (Carrier carrierObj:carriers) {

                if(carrierObj.getName().equalsIgnoreCase((carrier.getName()))){

                    throw new IllegalArgumentException("Carrier name must be unique.");
                }
        }

        UIDStore.addToStore(carrier.getUID(),carrier);
        carriers.add(carrier);
    }

    public int getCarrierCount(){

        return carriers.size();
    }

    public ArrayList<Carrier> getCarriers(){
        return carriers;
    }

    public Carrier getCarrierByName(String carrierName){

        //use iterators for safely removing the element.
        Iterator<Carrier> carrier = carriers.iterator();

        while(carrier.hasNext()){

            Carrier carrierObj = carrier.next();
            if(carrierObj.getName().equalsIgnoreCase((carrierName))){


                return carrierObj;
            }
        }

        throw new IllegalArgumentException("Carrier name not found.");

    }

    public void removeCarrier(String carrierName) throws IllegalArgumentException {

        //use iterators for safely removing the element.
        Iterator<Carrier> carrier = carriers.iterator();

        while(carrier.hasNext()){

            Carrier carrierObj = carrier.next();
            if(carrierObj.getName().equalsIgnoreCase((carrierName))){

                carrierObj.removeAllHubs();
                UIDStore.removeFromStore(carrierObj.getUID());
                carrier.remove();
                return;
            }
        }

        throw new IllegalArgumentException("Carrier name not found.");
    }

    public void removeAllCarriers()  {

        //use iterators for safely removing the element.
        Iterator<Carrier> carrier = carriers.iterator();

        while(carrier.hasNext()){

            Carrier carrierObj = carrier.next();
            carrierObj.removeAllHubs();
            UIDStore.removeFromStore(carrierObj.getUID());
            carrier.remove();

        }

    }

}

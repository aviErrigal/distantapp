package com.errigal.bo;

import com.errigal.util.UIDStore;
import com.errigal.util.Unit;

import java.util.ArrayList;
import java.util.Iterator;

/**
 *
 * Carrier here is a logical Unit
 * A Network can have multiple Carriers
 * A carrier can have multiple Hubs(which are physical units)
 * Created by avi on 06/06/2017.
 */
public class Carrier extends Unit implements Comparable<Carrier>{


    private ArrayList<Hub> hubs = new ArrayList<>();


    public Carrier(String carrierName) {

        this.setUID(UIDStore.generateUID());
        this.setName(carrierName);

    }

    public int getHubCount() {
        return hubs.size();
    }

    public ArrayList<Hub> getHubs() {

        return this.hubs;
    }

    public Hub getHubByName(String hubName) {

        //use iterators for safely removing the element.
        Iterator<Hub> hub = hubs.iterator();

        while (hub.hasNext()) {

            Hub hubObj = hub.next();
            if (hubObj.getName().equalsIgnoreCase((hubName))) {

                return hubObj;
            }
        }

        throw new IllegalArgumentException("Hub name not found.");

    }

    public void addHub(Hub hub) throws IllegalArgumentException {

        if (hub.getName().isEmpty() || hub.getName().trim().equals("")) {

            throw new IllegalArgumentException("Invalid Hub name.");
        }


        for (Hub hubObj : hubs) {

            if (hubObj.getName().equalsIgnoreCase((hub.getName()))) {

                throw new IllegalArgumentException("Hub name must be unique.");
            }
        }

        UIDStore.addToStore(hub.getUID(), hub);
        hubs.add(hub);
    }

    public void removeHub(String hubName) throws IllegalArgumentException {

        //use iterators for safely removing the element.
        Iterator<Hub> hub = hubs.iterator();

        while (hub.hasNext()) {

            Hub hubObj = hub.next();
            if (hubObj.getName().equalsIgnoreCase((hubName))) {

                hubObj.removeAllNodes();
                hubObj.clearAllAlarms();
                UIDStore.removeFromStore(hubObj.getUID());
                hub.remove();
                return;
            }
        }

        throw new IllegalArgumentException("Hub name not found.");
    }

    public void removeAllHubs() {

        //use iterators for safely removing the element.
        Iterator<Hub> hub = hubs.iterator();

        while (hub.hasNext()) {

            Hub hubObj = hub.next();
            hubObj.removeAllNodes();
            hubObj.clearAllAlarms();
            UIDStore.removeFromStore(hubObj.getUID());
            hub.remove();

        }

    }

    @Override
    public boolean equals(Object obj) {

        if (obj == this) {
            return true;
        }
        if (obj == null || obj.getClass() != this.getClass()) {
            return false;
        }
        Carrier carrier = (Carrier) obj;
        return  this.getUID() == carrier.getUID() && this.getName() == carrier.getName();
    }


    /**
     *
     * The hashCode method is inspired by method described in the book, Effective Java
     * @return computedHash
     */
    @Override
    public int hashCode() {
        int result = 17;
        result = 31 * result + this.getUID().hashCode();
        result = 31 * result + this.getName().hashCode();
        result = 31 * result + this.getHubCount();
        return result;
    }

    @Override
    public int compareTo(Carrier carrier)
    {
        return this.getName().compareTo(carrier.getName());
    }
}

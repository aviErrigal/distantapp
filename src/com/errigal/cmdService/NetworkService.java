package com.errigal.cmdService;

import com.errigal.bo.Carrier;
import com.errigal.bo.Network;
import com.errigal.util.UnitIterator;

/**
 * This is a cmdService to interact with Network
 * It supports
 * - Listing
 * Created by avi on 07/06/2017.
 */
public class NetworkService extends DistAntService{

    public NetworkService(Network network){
        super(network);
    }

    public void processList(String cmd){

        UnitIterator.Iterate(network,true,true,true);

    }



}

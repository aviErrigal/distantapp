package com.errigal.cmdService;

import com.errigal.bo.Carrier;
import com.errigal.bo.Hub;
import com.errigal.bo.Network;
import com.errigal.bo.Node;

/**
 * This is a cmdService to interact with Nodes
 * It supports
 * - Adding
 * - Removing
 * - Renaming
 * - Listing
 * Created by avi on 07/06/2017.
 */
public class NodeService extends DistAntService{

    public NodeService(Network network){
        super(network);
    }

    public void processRemove(String cmd) {

        cmd = cmd.trim();
        String cmdArr[] = cmd.split("/");

        if (cmdArr.length < 3) {

            System.out.println("Invalid Parameter Length");
            return;
        }

        Carrier carrier;

        try {

            carrier = network.getCarrierByName(cmdArr[0].trim());
        } catch (Exception e) {

            System.out.println(e.getMessage());
            return;
        }

        Hub hub;

        try {

            hub = carrier.getHubByName(cmdArr[1].trim());
        } catch (Exception e) {

            System.out.println(e.getMessage());
            return;
        }

        try {

            hub.removeNode(cmdArr[2]);
        } catch (Exception e) {

            System.out.println(e.getMessage());
            return;
        }

        System.out.println("Node removed Successfully.");


    }

    public void processRename(String cmd){
        cmd = cmd.trim();
        String cmdArr[] = cmd.split("/");

        if(cmdArr.length<3){

            System.out.println("Invalid Parameter Length");
            return;
        }

        Carrier carrier;

        try{

            carrier = network.getCarrierByName(cmdArr[0].trim());
        }catch(Exception e){

            System.out.println(e.getMessage());
            return;
        }

        Hub hub;

        try{

            hub = carrier.getHubByName(cmdArr[1].trim());
        }catch (Exception e){

            System.out.println(e.getMessage());
            return;
        }

        String nameArray[] = cmdArr[2].split(":");

        if(nameArray.length<2){

            System.out.println("Invalid Parameter Length. new Node name required");
            return;
        }

        Node node;

        try {
            node = hub.getNodeByName(nameArray[0]);

        }catch (IllegalArgumentException e){

            System.out.println(e.getMessage());
            return;
        }

        if(nameArray[1].trim().isEmpty() || nameArray[1].trim().equals("")){
            System.out.println("New node name not valid.");
        }

        node.setName(nameArray[1].trim());
    }

    public void processList(String cmd) {

        cmd = cmd.trim();
        String cmdArr[] = cmd.split("/");

        if (cmdArr.length < 2) {

            System.out.println("Invalid Parameter Length");
            return;
        }

        Carrier carrier;

        try {

            carrier = network.getCarrierByName(cmdArr[0].trim());
        } catch (Exception e) {

            System.out.println(e.getMessage());
            return;
        }

        Hub hub;

        try {

            hub = carrier.getHubByName(cmdArr[1].trim());
        } catch (Exception e) {

            System.out.println(e.getMessage());
            return;
        }

        if(hub.getNodeCount()==0){
            System.out.println("No Node has been added yet.");
            return;
        }
        hub.getNodes().forEach((node)->{

            System.out.println(node.getName());
        });

    }


    public void processAdd(String cmd){

        cmd = cmd.trim();
        String cmdArr[] = cmd.split("/");

        if(cmdArr.length<3){

            System.out.println("Invalid Parameter Length");
            return;
        }

        Carrier carrier;

        try{

            carrier = network.getCarrierByName(cmdArr[0].trim());
        }catch(Exception e){

            System.out.println(e.getMessage());
            return;
        }

        Hub hub;

        try{

            hub = carrier.getHubByName(cmdArr[1].trim());
        }catch (Exception e){

            System.out.println(e.getMessage());
            return;
        }

        String nameIDArr[] = cmdArr[2].split(":");

        if(nameIDArr.length<2){

            System.out.println("Invalid Parameter Length. Unique ID required");
            return;
        }

        System.out.println(addNode(hub,nameIDArr[1].trim(),nameIDArr[0].trim()));

    }

    public String addNode(Hub hub, String uid,String HubName){

        try {
            hub.addNode(new Node(uid,HubName));

        }catch (IllegalArgumentException e){
            return e.getMessage();
        }

        return "Node Added Successfully.";
    }


}

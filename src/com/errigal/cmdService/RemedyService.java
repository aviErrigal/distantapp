package com.errigal.cmdService;

import com.errigal.bo.*;
import com.errigal.util.ObservableUnit;
import com.errigal.util.UIDStore;
import com.errigal.util.UnitIterator;

/**
 * This is a cmdService to interact with Remedies
 * It supports
 * - Listing
 * Created by avi on 07/06/2017.
 */
public class RemedyService extends DistAntService{

    public RemedyService(Network network){
        super(network);
    }


    public void processList(String cmd){
        cmd = cmd.trim();


        String cmdArr[] = cmd.split("/");

        //make sure the length is enough for a node
        if(cmdArr.length<2){

            System.out.println("Invalid Parameter Length");
            return;
        }

        Carrier carrier;

        try{

            carrier = network.getCarrierByName(cmdArr[0].trim());
        }catch(Exception e){

            System.out.println(e.getMessage());
            return;
        }

        Hub hub;

        String hubName = cmdArr[1].trim();

        try{

            hub = carrier.getHubByName(hubName);
        }catch (Exception e){

            System.out.println(e.getMessage());
            return;
        }



        //specific hub
        if (cmdArr.length==2){
            //we know alarm has to be fetched from a hub.
            showRemedies(hub);
            return;
        }

        Node node;
        String nodeName = cmdArr[2].trim();
        try{

            node = hub.getNodeByName(nodeName);
        }catch (Exception e){

            System.out.println(e.getMessage());
            return;
        }

        showRemedies(node);

    }


    public void showRemedies(ObservableUnit unit){

        if( unit.getAlarms().size()==0){
            System.out.println("No remedies found for the unit:"+unit.getName());
            return;
        }
        unit.getAlarms().forEach((String k, Alarm alarm) ->{

            System.out.println("UID: "+k+" Remedy: "+alarm.getValue());
        });
    }

}

package com.errigal.cmdService;

import com.errigal.bo.Carrier;
import com.errigal.bo.Hub;
import com.errigal.bo.Network;

/**
 * This is a cmdService to interact with Hubs
 * It supports
 * - Adding
 * - Removing
 * - Renaming
 * - Listing
 * Created by avi on 07/06/2017.
 */
public class HubService extends DistAntService{

    public HubService(Network network){
        super(network);
    }


    public void processRemove(String cmd){


        cmd = cmd.trim();
        String cmdArr[] = cmd.split("/");

        if(cmdArr.length<2){

            System.out.println("Invalid Parameter Length");
            return;
        }

        Carrier carrier;

        try{

            carrier = network.getCarrierByName(cmdArr[0]);
        }catch(Exception e) {

            System.out.println(e.getMessage());
            return;
        }


        try{

            carrier.removeHub(cmdArr[1].trim());

        }catch(Exception e) {

            System.out.println(e.getMessage());
            return;
        }

        System.out.println("Hub Removed Successfully.");



    }

    public void processRename(String cmd){

        cmd = cmd.trim();
        String cmdArr[] = cmd.split("/");

        if(cmdArr.length<2){

            System.out.println("Invalid Parameter Length");
            return;
        }

        Carrier carrier;

        try{

            carrier = network.getCarrierByName(cmdArr[0].trim());
        }catch(Exception e){

            System.out.println(e.getMessage());
            return;
        }

        Hub hub;

        String nameArray[] = cmdArr[1].split(":");

        try{

            hub = carrier.getHubByName(nameArray[0].trim());
        }catch (Exception e){

            System.out.println(e.getMessage());
            return;
        }

        if(nameArray[1].trim().isEmpty() || nameArray[1].trim().equals("")){
            System.out.println("New Hub name not valid.");
        }

        hub.setName(nameArray[1].trim());

    }

    public void processList(String cmd){

        cmd = cmd.trim();

        if(cmd.isEmpty() || cmd.length()<1){

            System.out.println("Invalid Parameter Length");
            return;
        }

        Carrier carrier;

        try{

            carrier = network.getCarrierByName(cmd);
        }catch(Exception e){

            System.out.println(e.getMessage());
            return;
        }


        if(carrier.getHubCount()==0){
            System.out.println("No Hub has been added yet.");
            return;
        }
        carrier.getHubs().forEach((hub)->{

            System.out.println(hub.getName());
        });

    }


    public void processAdd(String cmd){

        cmd = cmd.trim();
        String cmdArr[] = cmd.split("/");

        if(cmdArr.length<2){

            System.out.println("Invalid Parameter Length");
            return;
        }

        Carrier carrier;

        try{

            carrier = network.getCarrierByName(cmdArr[0]);
        }catch(Exception e){

            System.out.println(e.getMessage());
            return;
        }

        String nameIDArr[] = cmdArr[1].split(":");

        if(nameIDArr.length<2){

            System.out.println("Invalid Parameter Length. Unique ID required");
            return;
        }

        System.out.println(addHub(carrier,nameIDArr[1].trim(),nameIDArr[0].trim()));

    }

    private String addHub(Carrier carrier, String uid,String HubName){

        try {
            carrier.addHub(new Hub(uid,HubName));

        }catch (IllegalArgumentException e){
            return e.getMessage();
        }

        return "Hub Added Successfully.";
    }


}

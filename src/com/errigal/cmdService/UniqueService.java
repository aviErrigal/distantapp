package com.errigal.cmdService;

import com.errigal.bo.Network;
import com.errigal.util.UIDStore;
import com.errigal.util.Unit;
import com.errigal.util.UnitIterator;

/**
 * This is a cmdService to interact with Units using their UniqueID
 * It supports
 * - Listing
 * Created by avi on 07/06/2017.
 */
public class UniqueService extends DistAntService{

    public UniqueService(Network network){
        super(network);
    }

    public void processList(String cmd){

        try{

        Unit unitObj = UIDStore.getUnit(cmd.trim());
        UnitIterator.Iterate(unitObj,true,true,true);

        }catch (Exception e){

            System.out.println(e.getMessage());
        }

    }



}

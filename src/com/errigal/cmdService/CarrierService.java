package com.errigal.cmdService;

import com.errigal.bo.Carrier;
import com.errigal.bo.Network;

/**
 * This is a cmdService to interact with Carriers
 * It supports
 * - Adding
 * - Removing
 * - Renaming
 * - Listing
 * Created by avi on 07/06/2017.
 */
public class CarrierService extends DistAntService{

    public CarrierService(Network network){
        super(network);
    }


    public void processRemove(String carrierName){

        try {
            network.removeCarrier(carrierName.trim());

        }catch (IllegalArgumentException e){
            System.out.println(e.getMessage());
            return;
        }

         System.out.println("Carrier Removed Successfully.");
    }

    public void processRename(String cmd){

        String nameArr[] = cmd.trim().split(":");

        if(nameArr.length<2){
            System.out.println("Invalid Parameter Length");
            return;
        }


        Carrier carrier;
        try{

            carrier = network.getCarrierByName(nameArr[0].trim());
        }catch (Exception e){

            System.out.println(e.getMessage());
            return;
        }

        if(nameArr[1].trim().isEmpty() || nameArr[1].trim().equals("")){
            System.out.println("New Carrier name not valid.");
        }

        carrier.setName(nameArr[1].trim());
    }

    public void processList(String cmd){

        if(network.getCarrierCount()==0){
            System.out.println("No carrier has been added yet.");
            return;
        }
        network.getCarriers().forEach((carrier)->{

            System.out.println(carrier.getName());
        });

    }

    public void processAdd(String cmd){

        System.out.println(addCarrier(cmd.trim()));

    }

    public String addCarrier(String carrierName){

        try {
            network.addCarrier(new Carrier(carrierName));

        }catch (IllegalArgumentException e){
            return e.getMessage();
        }

        return "Carrier Added Successfully.";
    }


}

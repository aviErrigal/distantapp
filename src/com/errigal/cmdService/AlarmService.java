package com.errigal.cmdService;

import com.errigal.bo.*;
import com.errigal.util.ObservableUnit;
import com.errigal.util.UIDStore;
import com.errigal.util.UnitIterator;

/**
 * This is a cmdService to interact with Alarms
 * It supports
 * - Adding
 * - Removing
 * - Listing
 * Created by avi on 07/06/2017.
 */
public class AlarmService extends DistAntService{

    public AlarmService(Network network){
        super(network);
    }


    public void processRemove(String cmd){

        //Expected I/P:  CarrierName/HubName/[:all|ALARM_TYPE|NodeName]/[:all|ALARM_TYPE]

        cmd = cmd.trim();

        String cmdArr[] = cmd.split("/");

        //make sure the length is enough for a node
        if(cmdArr.length<3){

            System.out.println("Invalid Parameter Length");
            return;
        }

        Carrier carrier;

        try{

            carrier = network.getCarrierByName(cmdArr[0].trim());
        }catch(Exception e){

            System.out.println(e.getMessage());
            return;
        }

        Hub hub;

        String hubName = cmdArr[1].trim();


        try{

            hub = carrier.getHubByName(hubName);
        }catch (Exception e){

            System.out.println(e.getMessage());
            return;
        }


        Alarm alarm;
        //remove from hub
        if (cmdArr.length==3){

            String alarmType = cmdArr[2].trim();
            if(alarmType.equals(":all")){
                hub.clearAllAlarms();
                System.out.println("Alarms have been cleared.");
                return;
            }

            //now we are dealing with a specific Alarmtype
            alarm = Alarm.valueOf(alarmType);
            if(alarm==null){
                System.out.println("Invalid Parameter Length. unknown ALARM_TYPE!");
                return;
            }

            //remove alarm by type.
            try {
                hub.removeAlarmByType(alarm);
                System.out.println("Alarm has been cleared.");

            }catch (Exception e){
                System.out.println(e.getMessage());

            }

            return;
        }

        Node node;
        String nodeName = cmdArr[2].trim();
        try{

            node = hub.getNodeByName(nodeName);
        }catch (Exception e){

            System.out.println(e.getMessage());
            return;
        }

        String alarmType = cmdArr[3].trim();
        if(alarmType.equals(":all")){
            node.clearAllAlarms();
            System.out.println("Alarms have been cleared.");
            return;
        }


        alarm = Alarm.valueOf(alarmType);
        if(alarm==null){
            System.out.println("Invalid Parameter Length. unknown ALARM_TYPE!");
            return;
        }

        //remove alarm by type.
        try {
            node.removeAlarmByType(alarm);
            System.out.println("Alarm has been cleared.");
        }catch (Exception e){
            System.out.println(e.getMessage());
        }


    }

    public void processList(String cmd){
        cmd = cmd.trim();

        //entire network
        if(cmd.equals(":all")){
            UnitIterator.Iterate(network,false,true,false);
            return;
        }

        String cmdArr[] = cmd.split("/");

        //make sure the length is enough for a node
        if(cmdArr.length<2){

            System.out.println("Invalid Parameter Length");
            return;
        }

        Carrier carrier;

        try{

            carrier = network.getCarrierByName(cmdArr[0].trim());
        }catch(Exception e){

            System.out.println(e.getMessage());
            return;
        }

        Hub hub;

        String hubName = cmdArr[1].trim();

        //carrier including,hubs and nodes
        if(hubName.equals(":all")){
            UnitIterator.Iterate(carrier,false,true,false);
            return;
        }

        try{

            hub = carrier.getHubByName(hubName);
        }catch (Exception e){

            System.out.println(e.getMessage());
            return;
        }



        //specific hub
        if (cmdArr.length>=2){
            //we know alarm has to be fetched from a hub.
            showAlarms(hub);
            //hub including nodes
            if(cmdArr.length>2 && cmdArr[2].trim().equals(":all")){
                UnitIterator.Iterate(hub,false,true,false);
                return;
            }
            return;
        }

        Node node;
        String nodeName = cmdArr[2].trim();
        try{

            node = hub.getNodeByName(nodeName);
        }catch (Exception e){

            System.out.println(e.getMessage());
            return;
        }

        showAlarms(node);

    }


    public void showAlarms(ObservableUnit unit){

        if( unit.getAlarms().size()==0){
            System.out.println("No alarms found for the unit:"+unit.getName());
            return;
        }
        unit.getAlarms().forEach((String k, Alarm alarm) ->{

            System.out.println("UID: "+k+" alarmType: "+alarm.getKey());
        });
    }

    public void processAdd(String cmd){


        cmd = cmd.trim();
        String cmdArr[] = cmd.split("/");

        //make sure the length is enough for a node
        if(cmdArr.length<2){

            System.out.println("Invalid Parameter Length");
            return;
        }

        Carrier carrier;

        try{

            carrier = network.getCarrierByName(cmdArr[0].trim());
        }catch(Exception e){

            System.out.println(e.getMessage());
            return;
        }

        Hub hub;

        //get rid of AlarmType from hubName if it exists.
        String hubName = cmdArr[1].split(":")[0].trim();

        try{

            hub = carrier.getHubByName(hubName);
        }catch (Exception e){

            System.out.println(e.getMessage());
            return;
        }


        String nameAlarmArr[] = cmdArr[cmdArr.length-1].split(":");
        Alarm alarm;

        if(nameAlarmArr.length<2){
            System.out.println("Invalid Parameter Length. ALARM_TYPE required");
            return;
        }else{


            String alarmType = nameAlarmArr[1].trim();
            alarm = Alarm.valueOf(alarmType);
            if(alarm==null){
                System.out.println("Invalid Parameter Length. unknown ALARM_TYPE!");
                return;
            }

        }

        if (cmdArr.length==2){
            //we know alarm has to be attached to a hub.
            System.out.println(addAlarmToHub(hub,alarm));
            return;
        }

        //we know we have enough parameters to attach alarm to a node.
        Node node;
        String nodeName = nameAlarmArr[0].trim();
        try{

            node = hub.getNodeByName(nodeName);
        }catch (Exception e){

            System.out.println(e.getMessage());
            return;
        }


        System.out.println(addAlarmToNode(node,alarm));

    }


    public  String addAlarmToHub(Hub hub, Alarm alarm){

        try {
                hub.attachAlarm(UIDStore.generateUID(),alarm);
        }catch (IllegalArgumentException e){
            return e.getMessage();
        }

        return "Alarm Attached to Hub Successfully.";
    }

    public  String addAlarmToNode(Node node, Alarm alarm){

        try {
            node.attachAlarm(UIDStore.generateUID(),alarm);
        }catch (IllegalArgumentException e){
            return e.getMessage();
        }

        return "Alarm Attached to Node Successfully.";
    }
}

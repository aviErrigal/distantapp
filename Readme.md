## How to Build
1. Open the project in the IDEA IDE.
2. Click on Build-> Build Artifacts
3. The project is configured to generate a DistAntApp.jar file
 
## Dependencies
- junit-4.12.jar
- hamcrest-core-1.3.jar
- gson-2.8.1.jar

## How to run
1. directly by running the Main file from the IDE
2. or by java -jar jarFilePath 

## Command Documentation

CLI design is mostly inspired by UNIX commands.

![](media/cmdFlow.png)

There are basically 7 commands:

1. list
2. add
3. remove
4. rename
5. import
6. export
7. quit

### Listing Units
To list the entire network:
```
list network
```
To list anything with uniqueID:
```
list unique uniqueID
```
To list the carriers:
```
list carrier
```
To list hubs on carrier:
```
list hub carrierName
```
To list nodes on a Hub:
```
list node carrierName/HubName
```
### Adding Units
To add a Carrier:
```
add carrier CarrierName
```
To add a Hub:
```
add hub CarrierName/NewHubName:UniqueID
```
To add a Node:
```
add node CarrierName/Hub Name/New Node Name:UnqiueID
```

### Removing Units
To Remove a Carrier:
```
remove carrier CarrierName
```
To remove a Hub:
```
remove hub CarrierName/HubName
```
To remove a Node:
```
remove node CarrierName/Hub Name/Node Name
```

### Renaming Units
To rename a Carrier:
```
rename carrier OldCarrierName:NewCarrierName
```
To rename a Hub:
```
rename hub CarrierName/OldHubName:NewHubName
```
To rename a Node:
```
rename node CarrierName/Hub Name/OldNodeName:NewNodeName
```


### Add Alarm

To Add alarm to a  Hub:
```
add alarm CarrierName/HubName:ALARM_TYPE
```
To Add alarm to a Node:
```
add alarm CarrierName/HubName/NodeName:ALARM_TYPE
```
following alarm types are available:
```
UNIT_UNAVAILABLE
OPTICAL_LOSS
DARK_FIBRE
POWER_OUTAGE
```

### View Alarms
To list all alarms on a network:
```
list alarm :all
```
To view alarms on all hubs in a carrier:
```
list alarm CarrierName/:all
```
To view alarms on a specific Hub:
```
list alarm CarrierName/HubName
```
To view all alarms in a hub including it's node(s):
```
list alarm CarrierName/HubName/:all
```
To view alarms on a specific Node:
```
list alarm CarrierName/HubName/NodeName
```

### Remove Alarm

To remove all alarms on a Hub:
```
remove alarm CarrierName/HubName/:all
```
To remove alarms by type on a Hub:
```
remove alarm CarrierName/HubName/ALARM_TYPE
```
To remove all alarms on a node:
```
remove alarm CarrierName/HubName/NodeName/:all
```
To remove alarms by type on a node:
```
remove alarm CarrierName/HubName/NodeName/ALARM_TYPE
```


### View Remedies

To view remedies for a  Hub:
```
list remedy CarrierName/HubName
```
To view remedies for a Node:
```
list remedy CarrierName/HubName/NodeName
```

### Import and Export

To import:
```
import /path/to/file.json
```
To export :
```
export /path/to/file.json
```

### Quit

To quit:
```
quit
```

## Testing
Unit Testing for all the above requirements has been covered.

There is an optional integration test which runs all the commands from the testSequence.txt file.

To run the test, Simply run the com.errigal.TestSequence main method from the project root.



###### Thank you.
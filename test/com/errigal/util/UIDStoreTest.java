package com.errigal.util;

import com.errigal.bo.Hub;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

/**
 * Created by avi on 06/06/2017.
 */
public class UIDStoreTest {
    @Test
    public void generateUID() throws Exception {

        UIDStore.reset();
        assertEquals("AT123",UIDStore.generateUID());
        UIDStore.addToStore(UIDStore.generateUID(),null);
        assertEquals("AT124",UIDStore.generateUID());
    }

    @Test
    public void addToStore() throws Exception {

        UIDStore.reset();
        UIDStore.addToStore("AAX",null);
        assertEquals(1,UIDStore.getStoreCount());

        //test unique UID
        try{
            UIDStore.addToStore("AAX",null);
            fail("Expected an illegalArgument Exception due to conflicting UIDs");

        } catch (IllegalArgumentException exceptionObj) {
            assertThat(exceptionObj.getMessage(), is("Given UID already exists."));
        }
    }

    @Test
    public void removeFromStore() throws Exception {

        UIDStore.reset();
        UIDStore.addToStore(UIDStore.generateUID(),null);
        UIDStore.addToStore(UIDStore.generateUID(),null);
        assertEquals(2,UIDStore.getStoreCount());

    }


}
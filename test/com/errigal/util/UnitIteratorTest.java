package com.errigal.util;

import com.errigal.bo.Alarm;
import com.errigal.bo.Hub;
import com.errigal.bo.Network;
import com.errigal.bo.Node;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by avi on 13/06/2017.
 */
public class UnitIteratorTest {


    @Test
    public void showAlarmsEmptyTest() throws Exception {

            Hub h =new Hub("aaa","bb");
            try {
                UnitIterator.showAlarms(h);
            }catch (Exception e){

                fail(e.getMessage());
            }

    }


    @Test
    public void showRemediesEmptyTest() throws Exception {

        Hub h =new Hub("aaa","bbx");
        try {
            UnitIterator.showRemedies(h);
        }catch (Exception e){

            fail(e.getMessage());
        }

    }


    @Test
    public void iterateEmptyTest() throws Exception {

        try{

            UnitIterator.Iterate(new Network(),true,true,true);
            UnitIterator.Iterate(new Node("uid00","n1"),true,true,true);
            UnitIterator.Iterate(new Hub("ccc","vvv"),true,true,true);
            Hub b = new Hub("cd","vvvd");
            b.attachAlarm(UIDStore.generateUID(), Alarm.UNIT_UNAVAILABLE);
            UnitIterator.Iterate(b,true,true,false);

        }catch (Exception e){

            fail(e.getMessage());
        }

    }


}
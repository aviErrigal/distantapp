package com.errigal.util;

import com.errigal.bo.*;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by avi on 09/06/2017.
 */
public class JSONDeserializerTest {
    @Test
    public void deserialize() throws Exception {

        UIDStore.reset();
        Network n =new Network();
        Hub hub = new Hub("hub221","hubname23");
        hub.addNode(new Node("uidHBT221","Node X2"));
        hub.addNode(new Node("uidHBT222","Node X3"));
        hub.attachAlarm("ss", Alarm.UNIT_UNAVAILABLE);
        Carrier c1 = new Carrier("c1");
        c1.addHub(hub);
        n.addCarrier(c1);
        String ret = JSONSerializer.Serialize(n);
        assertNotEquals(-1,ret.indexOf("uidHBT221"));
        int beforeResetCount = UIDStore.getStoreCount();
        UIDStore.reset();
        Network n2 = JSONDeserializer.Deserialize(ret);
        assertEquals(beforeResetCount,UIDStore.getStoreCount());

    }

}
package com.errigal.util;

import com.errigal.bo.*;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by avi on 09/06/2017.
 */
public class JSONSerializerTest {
    @Test
    public void serialize() throws Exception {

        UIDStore.reset();
        Network n =new Network();
        Hub hub = new Hub("hub221","hubname2");
        hub.addNode(new Node("uidHBT221","Node X2"));
        hub.addNode(new Node("uidHBT222","Node X3"));
        hub.attachAlarm("ss", Alarm.UNIT_UNAVAILABLE);
        Carrier c1 = new Carrier("c1");
        c1.addHub(hub);
        n.addCarrier(c1);
        String ret = JSONSerializer.Serialize(n);
        assertNotEquals(-1,ret.indexOf("uidHBT221"));

    }

}
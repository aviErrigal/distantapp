package com.errigal.cmdService;

import com.errigal.bo.Network;
import com.errigal.processor.CommandProcessor;
import com.errigal.util.UIDStore;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by avi on 13/06/2017.
 */
public class UniqueServiceTest {

    @Before
    public void setUp() throws Exception {

        CommandProcessor.debugMode = true;
        Network n = new Network();
        CommandProcessor.setDataObject(n);

    }


    @Test
    public void processList() throws Exception {

        UIDStore.reset();
        CommandProcessor.process("add carrier c1");
        CommandProcessor.process("add hub c1/h1:u1");
        UniqueService us =  new UniqueService(CommandProcessor.network);

        try {

            us.processList("");
            us.processList("u2");
            us.processList("u1");

        }catch (Exception e){

            fail(e.getMessage());
        }


    }

}
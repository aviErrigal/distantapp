package com.errigal.cmdService;

import com.errigal.bo.Hub;
import com.errigal.bo.Network;
import com.errigal.processor.CommandProcessor;
import com.errigal.util.UIDStore;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by avi on 13/06/2017.
 */
public class NodeServiceTest {

    @Before
    public void setUp() throws Exception {

        CommandProcessor.debugMode = true;
        Network n = new Network();
        CommandProcessor.setDataObject(n);

    }



    @Test
    public void processRemove() throws Exception {

        UIDStore.reset();
        NodeService ns = new NodeService(CommandProcessor.network);
        CommandProcessor.process("add  carrier c1 ");
        CommandProcessor.process("add carrier c32");
        CommandProcessor.process("add hub c32/h321:321");
        CommandProcessor.process("add hub c32/h321:321");
        CommandProcessor.process("add node c32/h321/n321:322");
        CommandProcessor.process("remove node c32/h321/n32TT");
        CommandProcessor.process("remove node c32/h321");
        ns.processRemove("c32/h321/n321");
        assertEquals(0,((Hub)UIDStore.getUnit("321")).getNodeCount());


    }

    @Test
    public void processRename() throws Exception {

        UIDStore.reset();
        NodeService ns = new NodeService(CommandProcessor.network);
        CommandProcessor.process("add  carrier c1 ");
        CommandProcessor.process("add carrier c32");
        CommandProcessor.process("add hub c32/h321:321");
        CommandProcessor.process("add node c32/h321/n321:322");
        ns.processList("");
        ns.processList("c32");
        ns.processList("c32/h321");
        CommandProcessor.process("rename node c32/h321/n32TT:ww");
        CommandProcessor.process("rename node ");
        CommandProcessor.process("rename node c32/h321/n32TT:ww");
        ns.processRename("c32/h321/n321:n32x");

        assertEquals("n32x",UIDStore.getUnit("322").getName());
    }

    @Test
    public void processList() throws Exception {


    }

    @Test
    public void processAdd() throws Exception {
    }

    @Test
    public void addNode() throws Exception {
    }

}
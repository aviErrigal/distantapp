package com.errigal.cmdService;

import com.errigal.bo.Carrier;
import com.errigal.bo.Network;
import com.errigal.processor.CommandProcessor;
import com.errigal.util.UIDStore;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by avi on 13/06/2017.
 */
public class RemedyServiceTest {


    @Before
    public void setUp() throws Exception {

        CommandProcessor.debugMode = true;
        Network n = new Network();
        CommandProcessor.setDataObject(n);

    }

    @Test
    public void processList() throws Exception {
        UIDStore.reset();
        CommandProcessor.process("add  carrier c1 ");
        CommandProcessor.process("add hub c1/h1:u2");
        CommandProcessor.process("add hub c1/h1/NN:u3");
        CommandProcessor.process("add alarm c1/h1:DARK_FIBRE");
        Carrier c = CommandProcessor.network.getCarrierByName("c1");
        assertEquals(1, c.getHubByName("h1").getAlarmCount());
        RemedyService rs = new RemedyService(CommandProcessor.network);
        try {
            rs.processList("c1/h1");
            rs.processList("c1/h1/n1");
            rs.processList("c1/h1/NN");
            rs.showRemedies(c.getHubByName("h1"));
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void showRemedies() throws Exception {
    }

}
package com.errigal.cmdService;

import com.errigal.bo.Network;
import com.errigal.processor.CommandProcessor;
import com.errigal.util.UIDStore;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by avi on 14/06/2017.
 */
public class CarrierServiceTest {

    @Before
    public void setUp() throws Exception {

        CommandProcessor.debugMode = true;
        Network n = new Network();
        CommandProcessor.setDataObject(n);

    }



    @Test
    public void processRemove() throws Exception {

        UIDStore.reset();
        CarrierService c =new CarrierService(CommandProcessor.network);
        c.addCarrier("c2");
        c.processRemove("c1");
        c.processRemove("c2");

        assertEquals(0,CommandProcessor.network.getCarrierCount());
    }

    @Test
    public void processRename() throws Exception {

        UIDStore.reset();
        CarrierService c =new CarrierService(CommandProcessor.network);
        c.addCarrier("c2");
        c.processRename("c1:c2");
        c.processRename("c2:");
        c.processRename("c2");
        c.processRename("c2:c3");
        assertEquals("c3",CommandProcessor.network.getCarrierByName("c3").getName());
    }

    @Test
    public void processList() throws Exception {

        UIDStore.reset();
        CarrierService c =new CarrierService(CommandProcessor.network);
        c.addCarrier("c2");
        c.processList("c2");
    }


}
package com.errigal.cmdService;

import com.errigal.bo.Carrier;
import com.errigal.bo.Hub;
import com.errigal.bo.Network;
import com.errigal.processor.CommandProcessor;
import com.errigal.util.UIDStore;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by avi on 14/06/2017.
 */
public class HubServiceTest {


    @Before
    public void setUp() throws Exception {

        CommandProcessor.debugMode = true;
        Network n = new Network();
        CommandProcessor.setDataObject(n);

    }



    @Test
    public void processRemove() throws Exception {

        UIDStore.reset();
        CarrierService cs =new CarrierService(CommandProcessor.network);
        cs.processAdd("c1");
        HubService hs = new HubService(CommandProcessor.network);
        hs.processRemove("");
        hs.processRemove("xx");
        hs.processAdd("c1/h1:u1");
        hs.processRemove("c1/h1");

        Carrier c = CommandProcessor.network.getCarrierByName("c1");
        assertEquals(0,c.getHubCount());


    }

    @Test
    public void processRename() throws Exception {
        UIDStore.reset();
        HubService hs = new HubService(CommandProcessor.network);
        CarrierService cs =new CarrierService(CommandProcessor.network);
        cs.processAdd("c1");
        hs.processAdd("c1/h1:u1");
        hs.processList("c1");
        hs.processList("cx");
        hs.processRename("c1:c2");
        hs.processRename("c1/h1:h2");
        assertEquals("h2",UIDStore.getUnit("u1").getName());
    }

    @Test
    public void processList() throws Exception {
    }

    @Test
    public void processAdd() throws Exception {
    }

}
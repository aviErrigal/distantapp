package com.errigal.cmdService;

import com.errigal.bo.*;
import com.errigal.processor.CommandProcessor;
import com.errigal.util.UIDStore;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by avi on 13/06/2017.
 */
public class AlarmServiceTest {

    @Before
    public void setUp() throws Exception {

        CommandProcessor.debugMode = true;
        Network n = new Network();
        CommandProcessor.setDataObject(n);

    }


    @Test
    public void processRemove() throws Exception {

        UIDStore.reset();
        CommandProcessor.process("add carrier c1");
        CommandProcessor.process("add hub c1/h1:u1");
        CommandProcessor.process("add alarm c1/h1:DARK_FIBRE");
        assertEquals(1, CommandProcessor.network.getCarrierByName("c1").getHubByName("h1").getAlarmCount());
        new AlarmService(CommandProcessor.network).processRemove(" c1/h1/:all");
        assertEquals(0, CommandProcessor.network.getCarrierByName("c1").getHubByName("h1").getAlarmCount());

    }

    @Test
    public void processList() throws Exception {

        UIDStore.reset();
        CommandProcessor.process("add  carrier c1");
        CommandProcessor.process("add hub c1/h1:u1");
        CommandProcessor.process("add alarm c1/h1:DARK_FIBRE");
        Carrier c = CommandProcessor.network.getCarrierByName("c1");
        assertEquals(1, c.getHubByName("h1").getAlarmCount());
        new AlarmService(CommandProcessor.network).processRemove(" c1/h1/:all");
        try {
            new AlarmService(CommandProcessor.network).processList(":all");
        } catch (Exception e) {
            fail(e.getMessage());
        }

    }

    @Test
    public void showAlarms() throws Exception {
        UIDStore.reset();
        CommandProcessor.process("add  carrier c1");
        CommandProcessor.process("add hub c1/h1:u2");

        CommandProcessor.process("add alarm c1/h1:DARK_FIBRE");
        Carrier c = CommandProcessor.network.getCarrierByName("c1");
        assertEquals(1, c.getHubByName("h1").getAlarmCount());
        new AlarmService(CommandProcessor.network).processRemove(" c1/h1/:all");
        try {
            new AlarmService(CommandProcessor.network).showAlarms(c.getHubByName("h1"));
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void processAdd() throws Exception {

        UIDStore.reset();
        CommandProcessor.process("add  carrier c1 ");
        CommandProcessor.process("add hub c1/h1:u2");
        CommandProcessor.process("add node c1/h1/n1:u22");
        Carrier c = CommandProcessor.network.getCarrierByName("c1");
        new AlarmService(CommandProcessor.network).processAdd("c1/h1:DARK_FIBRE");
        new AlarmService(CommandProcessor.network).processAdd("c1/h1/n1:DARK_FIBRE");
        assertEquals(1, c.getHubByName("h1").getAlarmCount());

    }

    @Test
    public void addAlarmToHub() throws Exception {
        UIDStore.reset();
        CommandProcessor.process("add  carrier c1");
        CommandProcessor.process("add hub c1/h1:u2");

        CommandProcessor.process("add alarm c1/h1:DARK_FIBRE");
        Carrier c = CommandProcessor.network.getCarrierByName("c1");
        Hub h = c.getHubByName("h1");
        new AlarmService(CommandProcessor.network).addAlarmToHub(h, Alarm.POWER_OUTAGE);
        assertEquals(2, h.getAlarmCount());


    }

    @Test
    public void addAlarmToNode() throws Exception {
        UIDStore.reset();
        CommandProcessor.process("add  carrier c1 ");
        CommandProcessor.process("add hub c1/h1:u2");
        CommandProcessor.process("add node c1/h1/n1:u22");
        Carrier c = CommandProcessor.network.getCarrierByName("c1");
        Hub h = c.getHubByName("h1");
        Node n = h.getNodeByName("n1");
        new AlarmService(CommandProcessor.network).addAlarmToNode(n, Alarm.POWER_OUTAGE);
        assertEquals(1, n.getAlarmCount());
    }

    @Test
    public void cmdTest() throws Exception {

        UIDStore.reset();
        AlarmService a = new AlarmService(CommandProcessor.network);
        CommandProcessor.process("add  carrier c1 ");
        CommandProcessor.process("add carrier c32");
        CommandProcessor.process("add hub c32/h321:321");
        CommandProcessor.process("remove hub c32/h321");
        CommandProcessor.process("list network");
        CommandProcessor.process("add hub c32/h321:321");
        CommandProcessor.process("add node c32/h321/n321:322");
        CommandProcessor.process("remove node c32/h321/n321");

        CommandProcessor.process("add alarm c32/h321:UNIT_UNAVAILABLE");
        CommandProcessor.process("add alarm c32/h321:DARK_FIBRE");
        CommandProcessor.process("add alarm c32/h321/n321:POWER_OUTAGE");
        CommandProcessor.process("list alarm c32/:all");
        CommandProcessor.process("remove alarm c32/h321/DARK_FIBRE");
        CommandProcessor.process("list alarm c32/:all");
        CommandProcessor.process("add alarm c32/h321/n321:DARK_FIBRE");
        CommandProcessor.process("remove alarm c32/h321/:all");
        CommandProcessor.process("list alarm c32/:all");
        CommandProcessor.process("remove alarm c32/h321/n321/POWER_OUTAGE");
        CommandProcessor.process("list alarm c32/:all");
        CommandProcessor.process("remove alarm c32/h321/n321/:all");
        CommandProcessor.process("remove alarm c32/h321/n321/:all");
        CommandProcessor.process("list alarm c32/:all");

        Carrier c = CommandProcessor.network.getCarrierByName("c32");
        Hub h = c.getHubByName("h321");
        assertEquals(0, h.getAlarmCount());

    }

}
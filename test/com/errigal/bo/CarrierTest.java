package com.errigal.bo;

import com.errigal.util.UIDStore;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

/**
 * Created by avi on 06/06/2017.
 */
public class CarrierTest {

    @Test
    public void getHubCount() throws Exception {

        UIDStore.reset();
        Carrier carrier = new Carrier("Vodafone");
        carrier.addHub(new Hub("uidCT001", "Hub X"));
        int count = carrier.getHubCount();
        assertEquals(1,count);
    }

    @Test
    public void getHubs() throws Exception {

        UIDStore.reset();
        Carrier carrier = new Carrier("Vodafone");
        carrier.addHub(new Hub("uidCT001", "Hub X"));
        assertEquals(1,carrier.getHubs().size());
    }

    @Test
    public void getHubByName() throws Exception {

        UIDStore.reset();
        Carrier carrier = new Carrier("Vodafone");
        carrier.addHub(new Hub("uidCT001", "Hub X"));
        assertEquals("Hub X",carrier.getHubByName("Hub X").getName());

    }


    @Test
    public void addHubTestUniqueID() throws Exception {

        UIDStore.reset();
        Carrier carrier = new Carrier("Vodafone");
        carrier.addHub(new Hub("uidCT001", "Hub X"));

        //test unique UID
        try{
            carrier.addHub(new Hub("uidCT001","TestHub"));
            fail("Expected an illegalArgument Exception due to conflicting UIDs");

        } catch (IllegalArgumentException exceptionObj) {
            assertThat(exceptionObj.getMessage(), is("Given UID already exists."));
        }


    }


    @Test
    public void addHubTestEmptyWithSpaces() throws Exception {

        UIDStore.reset();
        Carrier carrier = new Carrier("Vodafone");
        carrier.addHub(new Hub("uidCT001", "Hub X"));


        //test empty name but with spaces
        try{
            carrier.addHub(new Hub("uidCT004","    "));
            fail("Expected an illegalArgument Exception for spaced string");

        } catch (IllegalArgumentException exceptionObj) {
            assertThat(exceptionObj.getMessage(), is("Invalid Hub name."));
        }


    }


    @Test
    public void addHubTestEmpty() throws Exception {

        UIDStore.reset();
        Carrier carrier = new Carrier("Vodafone");
        carrier.addHub(new Hub("uidCT001", "Hub X"));

        //test empty name
        try{
            carrier.addHub(new Hub("uidCT003",""));
            fail("Expected an illegalArgument Exception for empty string");

        } catch (IllegalArgumentException exceptionObj) {
            assertThat(exceptionObj.getMessage(), is("Invalid Hub name."));
        }

    }

        @Test
    public void addHubTestDuplicate() throws Exception {

        UIDStore.reset();
        Carrier carrier = new Carrier("Vodafone");
        carrier.addHub(new Hub("uidCT001","Hub X"));

        //test duplicate name
        try {

            carrier.addHub(new Hub("uidCT002","Hub X"));
            fail("Expected an illegalArgument Exception");
        } catch (IllegalArgumentException exceptionObj) {
            assertThat(exceptionObj.getMessage(), is("Hub name must be unique."));
        }



    }

    @Test
    public void removeHub() throws Exception {

        UIDStore.reset();
        Carrier carrier = new Carrier("Vodafone");
        carrier.addHub(new Hub("uidCT001","Hub X"));
        assertEquals(1,carrier.getHubCount());
        carrier.removeHub("Hub X");
        assertEquals(0,UIDStore.getStoreCount());
        assertEquals(0,carrier.getHubCount());

    }

    @Test
    public void removeAllHubs() throws Exception {

        UIDStore.reset();
        Carrier carrier = new Carrier("VodafoneT");
        carrier.addHub(new Hub("uidCT001","Hub Y"));
        carrier.addHub(new Hub("uidCT003","Hub Z"));

        assertEquals(2,carrier.getHubCount());
        carrier.removeAllHubs();
        assertEquals(0,UIDStore.getStoreCount());
        assertEquals(0,carrier.getHubCount());

    }

    @Test
    public void equalsTest() throws Exception {

        UIDStore.reset();
        Carrier carrier = new Carrier("Vodafone");
        carrier.addHub(new Hub("uidCT001", "Hub X"));
        Carrier c2 = carrier;
        assert(c2.equals(carrier));

    }

    @Test
    public void hashCodeTest() throws Exception {

        UIDStore.reset();
        Carrier carrier = new Carrier("Voda");
        carrier.addHub(new Hub("uidCT001", "Hub X"));
        Carrier c2 = new Carrier("Voda");
        assertNotEquals(c2.hashCode(),carrier.hashCode());
    }


}
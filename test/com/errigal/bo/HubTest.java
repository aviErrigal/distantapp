package com.errigal.bo;

import com.errigal.util.UIDStore;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

/**
 * Created by avi on 06/06/2017.
 */
public class HubTest {



    @Test
    public void addNodeAlarmPropogationTest() throws Exception {

        UIDStore.reset();
        Hub hub = new Hub("hub1","hubnameT");
        hub.addNode(new Node("uidHBT1","Node X"));

        //test alarm propagation
        Node n = new Node("uidHBT22","Valid-Name2");
        hub.addNode(n);

        hub.attachAlarm(UIDStore.generateUID(),Alarm.UNIT_UNAVAILABLE);
        assertEquals(true,n.hasAlarm(Alarm.UNIT_UNAVAILABLE));

        //test alarm propagation, when node added after hub becomes UNIT_UNAVAILABLE
        Node n2 = new Node("uidHBT223","Valid-Name3");
        hub.addNode(n2);
        assertEquals(true,n2.hasAlarm(Alarm.UNIT_UNAVAILABLE));

        //test alarms which should not propogate
        hub.attachAlarm(UIDStore.generateUID(),Alarm.OPTICAL_LOSS);
        assertEquals(false,n2.hasAlarm(Alarm.OPTICAL_LOSS));

    }


    @Test
    public void addNodeUniqueIDTest() throws Exception {

        UIDStore.reset();
        Hub hub = new Hub("hub1", "hubname1");
        hub.addNode(new Node("uidHBT1", "Node X"));

        //test unique ID
        try{
            hub.addNode(new Node("uidHBT1","Valid-Name"));
            fail("Expected an illegalArgument Exception due to conflicting UIDs");

        } catch (IllegalArgumentException exceptionObj) {
            assertThat(exceptionObj.getMessage(), is("Given UID already exists."));
        }

    }

    @Test
    public void addNodeEmptyWithSpaceTest() throws Exception {

        UIDStore.reset();
        Hub hub = new Hub("hub1", "hubname1");
        hub.addNode(new Node("uidHBT1", "Node X"));

        //test empty name but with spaces
        try{
            hub.addNode(new Node("uidHBT4","   "));
            fail("Expected an illegalArgument Exception for spaced string");

        } catch (IllegalArgumentException exceptionObj) {
            assertThat(exceptionObj.getMessage(), is("Invalid Node name."));
        }
    }


    @Test
    public void addNodeEmptyTest() throws Exception {

        UIDStore.reset();
        Hub hub = new Hub("hub1", "hubname1");
        hub.addNode(new Node("uidHBT1", "Node X"));

        //test empty name
        try{
            hub.addNode(new Node("uidHBT3",""));
            fail("Expected an illegalArgument Exception for empty string");

        } catch (IllegalArgumentException exceptionObj) {
            assertThat(exceptionObj.getMessage(), is("Invalid Node name."));
        }
    }

    @Test
    public void addNode() throws Exception {

        UIDStore.reset();
        Hub hub = new Hub("hub1","hubname1");
        hub.addNode(new Node("uidHBT1","Node X"));

        //test unique name
        try {

            hub.addNode(new Node("uidHBT2","Node X"));
            fail("Expected an illegalArgument Exception");
        } catch (IllegalArgumentException exceptionObj) {
            assertThat(exceptionObj.getMessage(), is("Node name must be unique."));
        }



        //test alarm propagation
        Node n = new Node("uidHBT22","Valid-Name2");
        hub.addNode(n);

        hub.attachAlarm(UIDStore.generateUID(),Alarm.UNIT_UNAVAILABLE);
        assertEquals(true,n.hasAlarm(Alarm.UNIT_UNAVAILABLE));

        //test alarm propagation, when node added after hub becomes UNIT_UNAVAILABLE
        Node n2 = new Node("uidHBT223","Valid-Name3");
        hub.addNode(n2);
        assertEquals(true,n2.hasAlarm(Alarm.UNIT_UNAVAILABLE));

        //test alarms which should not propogate
        hub.attachAlarm(UIDStore.generateUID(),Alarm.OPTICAL_LOSS);
        assertEquals(false,n2.hasAlarm(Alarm.OPTICAL_LOSS));



    }

    @Test
    public void removeNode() throws Exception {


        UIDStore.reset();
        Hub hub = new Hub("hub221","hubname2");
        hub.addNode(new Node("uidHBT221","Node X2"));
        assertEquals(1,hub.getNodeCount());
        hub.removeNode("Node X2");
        assertEquals(0,UIDStore.getStoreCount());
        assertEquals(0,hub.getNodeCount());

    }

    @Test
    public void removeAllNodes() throws Exception {
        UIDStore.reset();
        Hub hub = new Hub("hub221","hubname2");
        hub.addNode(new Node("uidHBT221","Node X2"));
        hub.addNode(new Node("uidHBT222","Node X3"));
        assertEquals(2,hub.getNodeCount());
        hub.removeAllNodes();
        assertEquals(0,UIDStore.getStoreCount());
        assertEquals(0,hub.getNodeCount());
    }
    @Test
    public void attachAlarm() throws Exception {

        Hub hub = new Hub("hub2","hubname2");
        hub.addNode(new Node("uidHBT0010","Node X"));

        hub.attachAlarm(UIDStore.generateUID(),Alarm.DARK_FIBRE);
        hub.attachAlarm(UIDStore.generateUID(),Alarm.POWER_OUTAGE);

        assertEquals(2,hub.getAlarmCount());


    }

    @Test
    public void removeAlarm() throws Exception {

        Hub hub = new Hub("hub3","hubname3");
        hub.addNode(new Node("uidHBT001","Node A"));

        hub.attachAlarm(UIDStore.generateUID(),Alarm.DARK_FIBRE);
        String key = UIDStore.generateUID();
        hub.attachAlarm(key,Alarm.POWER_OUTAGE);

        assertEquals(2,hub.getAlarmCount());

        try{
            hub.removeAlarm("WRONG KEY");
            fail("Expected an illegalArgument Exception");

        } catch (IllegalArgumentException exceptionObj) {
            assertThat(exceptionObj.getMessage(), is("Given UID does not exist."));
        }

        hub.removeAlarm(key);
        assertEquals(1,hub.getAlarmCount());

    }

    @Test
    public void clearAllAlarms() throws Exception {

        Hub hub = new Hub("hub4","hubname4");
        hub.addNode(new Node("uidHBT002","Node y"));

        hub.attachAlarm(UIDStore.generateUID(),Alarm.OPTICAL_LOSS);
        hub.attachAlarm(UIDStore.generateUID(),Alarm.UNIT_UNAVAILABLE);
        hub.clearAllAlarms();
        String key = Alarm.UNIT_UNAVAILABLE.getKey();
        String val = Alarm.UNIT_UNAVAILABLE.getValue();
        assertEquals("Remedey for Unit Unavailable.",val);
        assertEquals(0,hub.getAlarmCount());


    }

    @Test
    public void getNodeCount() throws Exception {

        UIDStore.reset();
        Hub hub = new Hub("hub4","hubname4");
        hub.addNode(new Node("uidHBT002","Node y"));
        assertEquals(1,hub.getNodeCount());
    }

    @Test
    public void getNodes() throws Exception {

        UIDStore.reset();
        Hub hub = new Hub("hub4","hubname4");
        hub.addNode(new Node("uidHBT002","Node y"));
        assertEquals(1,hub.getNodes().size());
    }

    @Test
    public void getNodeByName() throws Exception {

        UIDStore.reset();
        Hub hub = new Hub("hub4","hubname4");
        hub.addNode(new Node("uidHBT002","Node y"));
        assertEquals("Node y",hub.getNodeByName("Node y").getName());
    }
}
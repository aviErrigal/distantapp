package com.errigal.bo;

import com.errigal.util.UIDStore;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

/**
 * Created by avi on 06/06/2017.
 */
public class NodeTest {

    @Test
    public void attachAlarm() throws Exception {

        Node node = new Node("uidNT001","node 1");


        node.attachAlarm(UIDStore.generateUID(),Alarm.DARK_FIBRE);
        node.attachAlarm(UIDStore.generateUID(),Alarm.POWER_OUTAGE);

        assertEquals(2,node.getAlarmCount());


    }

    @Test
    public void removeAlarm() throws Exception {

        Node node = new Node("uidNT002","node 2");

        node.attachAlarm(UIDStore.generateUID(),Alarm.DARK_FIBRE);
        String key = UIDStore.generateUID();
        node.attachAlarm(key,Alarm.POWER_OUTAGE);

        assertEquals(2,node.getAlarmCount());

        try{
            node.removeAlarm("WRONG KEY");
            fail("Expected an illegalArgument Exception");

        } catch (IllegalArgumentException exceptionObj) {
            assertThat(exceptionObj.getMessage(), is("Given UID does not exist."));
        }

        node.removeAlarm(key);
        assertEquals(1,node.getAlarmCount());

    }

    @Test
    public void clearAllAlarms() throws Exception {

        Node node = new Node("uidNT003","node 3");

        node.attachAlarm(UIDStore.generateUID(),Alarm.OPTICAL_LOSS);
        node.attachAlarm(UIDStore.generateUID(),Alarm.UNIT_UNAVAILABLE);
        node.clearAllAlarms();;

        assertEquals(0,node.getAlarmCount());


    }
}
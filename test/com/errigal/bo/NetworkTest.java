package com.errigal.bo;

import com.errigal.util.UIDStore;
import org.junit.Test;

import org.junit.Assert;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;

/**
 * Created by avi on 06/06/2017.
 */
public class NetworkTest {



    @Test
    public void addCarrierNAmeWithSpaceTest() throws Exception {

        Network network = new Network();
        network.addCarrier(new Carrier("Vodafone"));

        //test empty name but with spaces
        try{
            network.addCarrier(new Carrier("  "));
            fail("Expected an illegalArgument Exception for string with spaces");

        } catch (IllegalArgumentException exceptionObj) {
            assertThat(exceptionObj.getMessage(), is("Invalid Carrier name."));
        }
    }

    @Test
    public void addCarrierEmptySpaceTest() throws Exception {

        Network network = new Network();
        network.addCarrier(new Carrier("Vodafone"));
        //test empty name
        try{
            network.addCarrier(new Carrier(""));
            fail("Expected an illegalArgument Exception for empty string");

        } catch (IllegalArgumentException exceptionObj) {
            assertThat(exceptionObj.getMessage(), is("Invalid Carrier name."));
        }
    }


        @Test
    public void addCarrier() throws Exception {

        Network network = new Network();
        network.addCarrier(new Carrier("Vodafone"));

        try {

            network.addCarrier(new Carrier("Vodafone"));
            fail("Expected an illegalArgument Exception");
        } catch (IllegalArgumentException exceptionObj) {
            assertThat(exceptionObj.getMessage(), is("Carrier name must be unique."));
        }

    }

    @Test
    public void removeCarrier() throws Exception {

        UIDStore.reset();
        Network network = new Network();
        network.addCarrier(new Carrier("Vodafone"));
        assertEquals(1,network.getCarrierCount());
        network.removeCarrier("Vodafone");
        assertEquals(1,UIDStore.getStoreCount());
        assertEquals(0,network.getCarrierCount());

    }

    @Test
    public void removeAllHubs() throws Exception {

        UIDStore.reset();
        Network network = new Network();
        network.addCarrier(new Carrier("Vodafone"));
        network.addCarrier(new Carrier("AT&T"));
        assertEquals(2,network.getCarrierCount());
        network.removeAllCarriers();
        assertEquals(1,UIDStore.getStoreCount());
        assertEquals(0,network.getCarrierCount());

    }

    @Test
    public void getCarrierCount() throws Exception {

        UIDStore.reset();
        Network network = new Network();
        network.addCarrier(new Carrier("Vodafone"));
        network.addCarrier(new Carrier("AT&T"));
        assertEquals(2,network.getCarrierCount());
    }

    @Test
    public void getCarriers() throws Exception {


        UIDStore.reset();
        Network network = new Network();
        network.addCarrier(new Carrier("Vodafone2"));
        network.addCarrier(new Carrier("AT&T"));
        assertEquals(2,network.getCarriers().size());
    }

    @Test
    public void getCarrierByName() throws Exception {

        UIDStore.reset();
        Network network = new Network();
        network.addCarrier(new Carrier("Vodafone2"));
        network.addCarrier(new Carrier("ATT"));
        assertEquals("ATT",network.getCarrierByName("ATT").getName());
    }


}

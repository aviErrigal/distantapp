package com.errigal.processor;

import com.errigal.bo.Network;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by avi on 13/06/2017.
 */
public class RemoveCmdProcessorTest {
    @Before
    public void setUp() throws Exception {

        CommandProcessor.debugMode = true;
        Network n = new Network();
        CommandProcessor.setDataObject(n);

    }

    @Test
    public void processBadCommand() throws Exception {


        try{
            CommandProcessor.process("remove carrieD r");
            fail("Expected an Exception for invalid command");

        } catch (Exception e) {

        }
    }

    @Test
    public void process() throws Exception {

        CommandProcessor.debugMode = true;
        try {
            CommandProcessor.process("remove carrier c1");
        }catch(Exception e){

            fail(e.getMessage());
        }

    }
}
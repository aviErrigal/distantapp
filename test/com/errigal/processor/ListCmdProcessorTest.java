package com.errigal.processor;

import com.errigal.bo.Network;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by avi on 13/06/2017.
 */
public class ListCmdProcessorTest {


    @Before
    public void setUp() throws Exception {

        CommandProcessor.debugMode = true;
        Network n = new Network();
        CommandProcessor.setDataObject(n);

    }

    @Test
    public void processBadCommand() throws Exception {


        try{
            CommandProcessor.process("list NeeeeeE");
            fail("Expected an Exception for invalid command");

        } catch (Exception e) {

        }
    }

    @Test
    public void process() throws Exception {

        CommandProcessor.debugMode = true;
        try {
            CommandProcessor.process("list network");
        }catch(Exception e){

            fail(e.getMessage());
        }

    }


}
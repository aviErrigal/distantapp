package com.errigal.processor;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by avi on 13/06/2017.
 */
public class CmdProcessorInterfaceTest {
    @Test
    public void process() throws Exception {
        //does nothing
        CmdProcessorInterface.process("");
    }

    @Test
    public void capitalize() throws Exception {

        assertEquals("Hello",CmdProcessorInterface.capitalize("hello"));
    }

}
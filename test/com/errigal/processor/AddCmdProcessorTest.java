package com.errigal.processor;

import com.errigal.bo.Network;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by avi on 13/06/2017.
 */
public class AddCmdProcessorTest {
    @Before
    public void setUp() throws Exception {

        CommandProcessor.debugMode = true;
        Network n = new Network();
        CommandProcessor.setDataObject(n);

    }

    @Test
    public void processBadCommand() {


        try{
            CommandProcessor.process("add carrieTT");
            fail("Expected an Exception for invalid command");

        } catch (Exception e) {

        }
    }

    @Test
    public void process() throws Exception {

        CommandProcessor.debugMode = true;
        try {
            CommandProcessor.process("add carrier c1");
        }catch(Exception e){

            fail(e.getMessage());
        }

    }
}
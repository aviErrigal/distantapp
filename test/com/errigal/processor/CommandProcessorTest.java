package com.errigal.processor;

import com.errigal.bo.Network;
import com.errigal.bo.Node;
import org.junit.Before;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

/**
 * Created by avi on 13/06/2017.
 */
public class CommandProcessorTest {

    @Before
    public void setUp() throws Exception {

        CommandProcessor.debugMode = true;
        Network n = new Network();
        CommandProcessor.setDataObject(n);

    }

    @Test
    public void processBadCommand() throws Exception {


        try{
            CommandProcessor.process("lisssst network");
            fail("Expected an Exception for invalid command");

        } catch (Exception e) {


        }
    }

    @Test
    public void process() throws Exception {

        CommandProcessor.debugMode = true;
        try {
            CommandProcessor.process("list network");
        }catch(Exception e){

            fail(e.getMessage());
        }

    }

    @Test
    public void setDataObject() throws Exception {

        Network n = new Network();
        assertEquals(true,CommandProcessor.setDataObject(n));

    }

}
package com.errigal.processor;

import com.errigal.bo.Network;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by avi on 13/06/2017.
 */
public class QuitCmdProcessorTest {


    @Before
    public void setUp() throws Exception {

        CommandProcessor.debugMode = true;
        Network n = new Network();
        CommandProcessor.setDataObject(n);

    }

    @Test
    public void process() throws Exception {

        CommandProcessor.process("quit");
        assertEquals(true,CommandProcessor.exit);

    }


}
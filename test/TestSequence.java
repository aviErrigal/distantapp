import com.errigal.bo.Network;
import com.errigal.processor.CommandProcessor;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.Scanner;

/**
 * Created by avi on 08/06/2017.
 */
public class TestSequence {

    public static void main(String args[]){

        Network network = new Network();
        CommandProcessor.setDataObject(network);


        String line;
        try (
                InputStream fis = new FileInputStream("./testSequence.txt");
                InputStreamReader isr = new InputStreamReader(fis, Charset.forName("UTF-8"));
                BufferedReader br = new BufferedReader(isr);
        ) {
            while ((line = br.readLine()) != null) {

                System.out.println("$ "+line);
                CommandProcessor.process(line);

            }
        }catch (Exception e){

            System.out.println(e.getMessage());
        }


    }

}
